As a passionate developer and a football fan, currently working on this application which consolidates the data available open source to display the top ranked teams based on different parameters. Planned to enhance the project for predictive analytics using data mining techniques.

Tech stack :
Data layer      :  Single node Hadoop hosted on raspberry pi 
Front end       :  Angular 6
Backend        :  Spring boot
