package com.football.rankings;

import com.football.rankings.elasticsearch.ElasticIngestJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class FootBallRankingApplication implements CommandLineRunner {
	@Autowired
	private ElasticIngestJob elasticIngestJob;

	public static void  main(String args[])
	{
		SpringApplication.run(FootBallRankingApplication.class);
	}

	@Override
	public void run(String... args) throws Exception {
		//elasticIngestJob.ingestJob();
	}
}
