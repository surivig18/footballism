package com.football.rankings.aspect;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Slf4j
public class HadoopMonitoringAspect {

    @Pointcut("execution(* com.football.rankings.repository.*.get*(..))")
    public void hadoopExecutionDao(){}

    @SneakyThrows
    @Around("hadoopExecutionDao()")
    public Object logTiming(ProceedingJoinPoint proceedingJoinPoint)
    {
        long startTime = System.currentTimeMillis();
        log.info(String.valueOf(startTime));
        Object returnVal =   proceedingJoinPoint.proceed();
        long endTime = System.currentTimeMillis();
        log.info(String.valueOf(endTime));
        log.info("Total time taken :: " + proceedingJoinPoint.getSignature().getName() +  " ::  " +String.valueOf(endTime -startTime));

        return  returnVal;

    }
}
