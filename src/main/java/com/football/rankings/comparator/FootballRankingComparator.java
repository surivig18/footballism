package com.football.rankings.comparator;

import java.util.Comparator;

import com.football.rankings.model.RankingModel;

public class FootballRankingComparator implements Comparator<RankingModel>{

	@Override
	public int compare(RankingModel rankingModel1, RankingModel rankingModel2) {
		if(rankingModel1.getRank() > rankingModel2.getRank())
		{
			return 1;
		}
		else {
			return -1;
		}

	}

}
