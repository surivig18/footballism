package com.football.rankings.config;

import org.apache.hadoop.mapred.JobConf;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HdfsConfig {


    @Bean(name = "jobConf")
    public JobConf getHdfsConfig(){
        org.apache.hadoop.conf.Configuration configuration = new org.apache.hadoop.conf.Configuration();
        configuration.set("fs.defaultFS", "hdfs://raspsuria1:9000");
        return new JobConf(configuration);
    }

    @ConditionalOnProperty(name="datanode", havingValue = "hdfs")
    @Bean(name = "sparkContext")
    public JavaSparkContext getSparkContext(){
        JavaSparkContext sc = new JavaSparkContext(getSparkConf());
        sc.hadoopConfiguration().set("fs.defaultFS", "hdfs://raspsuria1:9000");
        return sc;
    }

    @ConditionalOnProperty(name="datanode", havingValue = "local")
    @Bean(name = "sparkContext")
    public JavaSparkContext sparkContext()
    {
        SparkConf sparkConf = getSparkConf();
        sparkConf.set("es.index.auto.create", "true");
        sparkConf.set("spark.es.nodes","localhost");
        sparkConf.set("spark.es.port","9200");
        JavaSparkContext jsc = new JavaSparkContext(sparkConf);

        return jsc;
    }

    public SparkConf getSparkConf()
    {
        SparkConf sparkConf = new SparkConf().setAppName("footballranks").setMaster("local");
        sparkConf.set("spark.driver.allowMultipleContexts", "true");
        return sparkConf;
    }
}
