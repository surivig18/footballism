package com.football.rankings.controller;

import java.io.File;
import java.util.Collections;
import java.util.List;

import com.football.rankings.elasticsearch.query.TeamsQuerySearch;
import com.football.rankings.model.MatchesModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.football.rankings.comparator.FootballRankingComparator;
import com.football.rankings.exception.HadoopConnectivityException;
import com.football.rankings.model.BaseModel;
import com.football.rankings.model.RankingModel;
import com.football.rankings.repository.RankingRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@Controller
public class FootballRankingController 
{
	
	
    private static final Logger logger = LogManager.getLogger(FootballRankingController.class);
    
    @Autowired
    private RankingRepository rankingRepository;

    @Autowired
	private TeamsQuerySearch teamsQuerySearch;

    @CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.GET,path = "/football/ranks", produces = "application/json")
    public  ResponseEntity<BaseModel> retrieveRankings() 
    {

    	logger.info("Reading from source");
    	List<RankingModel> rankingModelList;
    	try {
    		rankingModelList = rankingRepository.getRankingList();

    		logger.info("Ranking model size ::" + rankingModelList.size());
    		BaseModel baseModel = new BaseModel();
    		baseModel.setRankingModels(rankingModelList);
    		return new ResponseEntity<>(baseModel,HttpStatus.OK);
    	} catch (HadoopConnectivityException e) {
    		e.printStackTrace();
			logger.error("Connectivity exception :: " + e.getMessage());
    	}
		return null;


    }

    @GetMapping(path = "/football/matches", produces = "application/json")
    public ResponseEntity<BaseModel> retrieveMatchesList(){
    	ResponseEntity<BaseModel> baseModelResponseEntity = null;
    	logger.info("Matches list");

		try {
			BaseModel baseModel = new BaseModel();
			List<MatchesModel> matchesModels = rankingRepository.getMatchesFromHadoop();
			logger.info("Size of matches mode" + matchesModels.size());
			baseModel.setMatchesModels(matchesModels);
			baseModelResponseEntity = new ResponseEntity<BaseModel>(baseModel,HttpStatus.OK);
			return baseModelResponseEntity;
		} catch (HadoopConnectivityException e) {
		}
		return null;
	}

	@GetMapping(path = "/football/matches/Liverpool", produces = "application/json")
	public ResponseEntity<BaseModel> getLiverpoolMatches()
	{
		ResponseEntity<BaseModel> baseModelResponseEntity = null;

		List<MatchesModel> liverpoolList =  teamsQuerySearch.getDataByTeamName("Liverpool");
			BaseModel baseModel = new BaseModel();
		baseModel.setMatchesModels(liverpoolList);
		baseModelResponseEntity = new ResponseEntity<BaseModel>(baseModel,HttpStatus.OK);
		return baseModelResponseEntity;

	}
    static private void sortRankingList(List<RankingModel> rankingModels)
	{
		rankingModels.stream().sorted(new FootballRankingComparator());
	}

}

