package com.football.rankings.elasticsearch;

import com.football.rankings.exception.HadoopConnectivityException;
import com.football.rankings.model.MatchesModel;
import com.football.rankings.model.RankingModel;
import com.football.rankings.repository.RankingRepository;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.elasticsearch.spark.rdd.api.java.JavaEsSpark;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class ElasticIngestJob {


    private static final Logger logger = LoggerFactory.getLogger(ElasticIngestJob.class);

    @Autowired
    private RankingRepository rankingRepository;

    @Qualifier("sparkContext")
    @Autowired
    JavaSparkContext localSparkContext;

    public ElasticIngestJob(){

    }


    public void ingestJob(){


        List<MatchesModel> matchesModels = null;
        try {
            matchesModels = rankingRepository.getMatchesFromHadoop();
        } catch (HadoopConnectivityException e) {
                logger.info("ElasticInjectJob :: connectivity failure" + e.getMessage());
            }
        JavaRDD<MatchesModel> javaRDD = localSparkContext.parallelize(matchesModels);
        JavaEsSpark.saveToEs(javaRDD, "football/matches");
    }

}


