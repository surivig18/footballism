package com.football.rankings.elasticsearch.query;

import com.football.rankings.model.MatchesModel;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.DoubleType;
import org.elasticsearch.spark.sql.api.java.JavaEsSparkSQL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class TeamsQuerySearch {

    @Qualifier("sparkContext")
    @Autowired
    JavaSparkContext localSparkContext;

    public List<MatchesModel> getDataByTeamName(String team)
    {
        SQLContext sql = new SQLContext(localSparkContext);
        Dataset<Row> resultSet = JavaEsSparkSQL.esDF(sql, "football/matches" ,"?q=" + team);
        resultSet.show();

        return transformDataSet(resultSet);
    }

    public List<MatchesModel> transformDataSet(Dataset<Row> resultSet)
    {
        Dataset<MatchesModel> matchesModelDataset = (Dataset<MatchesModel>)resultSet.
                withColumn("date", new Column("date").cast(DataTypes.StringType)).
                withColumn("leagueId", new Column("leagueId").cast(DataTypes.IntegerType)).
                withColumn("score1", new Column("score1").cast(DataTypes.IntegerType)).
                withColumn("score2", new Column("score2").cast(DataTypes.IntegerType)).
                as(Encoders.bean(MatchesModel.class));
        List<MatchesModel> matchesModelList =  matchesModelDataset.sort(new Column("leagueId")).collectAsList();
        return matchesModelList;
    }
}
