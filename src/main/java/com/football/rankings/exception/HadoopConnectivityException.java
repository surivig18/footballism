package com.football.rankings.exception;

public class HadoopConnectivityException extends Throwable{

	
	private static final long serialVersionUID = 1L;
	
		public HadoopConnectivityException(String message) {
			super(message);
		}

}
