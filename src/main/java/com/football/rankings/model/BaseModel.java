package com.football.rankings.model;

import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;
import java.util.List;

@Data
public class BaseModel implements Serializable {
	
	private List<RankingModel> rankingModels;

	private List<MatchesModel> matchesModels;


}
