package com.football.rankings.model;

import com.football.rankings.annotation.MappingField;
import lombok.Data;
import lombok.Generated;

import java.io.Serializable;

@Data
public class MatchesModel implements Serializable {

    private String date;
    @MappingField("league_id")
    private int leagueId;
    private String league;
    private String team1;
    private String team2;
    private double spi1;
    private double spi2;
    private double prob1;
    private double prob2;
    private double probtie;
    @MappingField("proj_score1")
    private double projScore1;
    @MappingField("proj_score2")
    private double projScore2;
    private double importance1;
    private double importance2;
    private int score1;
    private int score2;
    private double xg1;
    private double xg2;
    private double nsxg1;
    private double nsxg2;
    @MappingField("adj_score1")
    private double adjScore1;
    @MappingField("adj_score2")
    private double adjScore2;
}
