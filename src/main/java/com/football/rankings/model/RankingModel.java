package com.football.rankings.model;

import java.io.Serializable;
import java.util.Objects;

public class RankingModel implements Serializable {
	private int rank;
	private int prevRank;
	private String name; 
	private String league; 
	private double off; 
	private double def; 
	private double spi;
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public int getPrevRank() {
		return prevRank;
	}
	public void setPrevRank(int prevRank) {
		this.prevRank = prevRank;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLeague() {
		return league;
	}
	public void setLeague(String league) {
		this.league = league;
	}
	public double getOff() {
		return off;
	}
	public void setOff(double off) {
		this.off = off;
	}
	public double getDef() {
		return def;
	}
	public void setDef(double def) {
		this.def = def;
	}
	public double getSpi() {
		return spi;
	}
	public void setSpi(double spi) {
		this.spi = spi;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		RankingModel that = (RankingModel) o;
		return rank == that.rank &&
				prevRank == that.prevRank &&
				Double.compare(that.spi, spi) == 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(rank, prevRank, spi);
	}
}
