package com.football.rankings.repository;

import java.io.IOException;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.football.rankings.exception.HadoopConnectivityException;

@Slf4j
@Component
public class BaseRepositoryImpl {

	@Value("${hdfs-location}")
	private String hdfsLocation ;

	@Value("${datanode}")
	String dataNode;

	@Autowired
	JavaSparkContext sparkContext;

	private static final Logger logger = LogManager.getLogger(BaseRepositoryImpl.class);

	public String getSourceDataFromHadoop(String path) throws  HadoopConnectivityException
	{
		Configuration configuration = new Configuration();
		configuration.set("fs.defaultFS", "hdfs://raspsuria1:9000");
		FileSystem fs;
		try {
			fs = FileSystem.get(configuration);
			Path filePath = new Path(hdfsLocation + path);
			FSDataInputStream fsDataInputStream = fs.open(filePath);
			String out= IOUtils.toString(fsDataInputStream, "UTF-8");
			fsDataInputStream.close();
			return out;
		}
		
		catch (IOException e) {
			logger.error("Error while connecting to Hadoop " , e);
			
			throw new HadoopConnectivityException(e.getLocalizedMessage());
		}
		catch (Exception e) {
			logger.error("Exception while connectivty to hadoop");
			throw new HadoopConnectivityException(e.getLocalizedMessage());
		}

	}

public JavaRDD<String> getSourceDataUsingSpark(String path)
{
	try {
		String dataSourceLocation =  dataNode.equalsIgnoreCase("local") ? "/Users/sindusuria/dev/data/" + path : hdfsLocation + path;
		JavaRDD<String> javaRDD = sparkContext.textFile(dataSourceLocation);
		return javaRDD;
	}
	catch (Exception e)
	{
		log.error("Spark job" + e.getMessage());
		return null;
	}
}

}
