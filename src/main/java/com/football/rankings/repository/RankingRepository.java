package com.football.rankings.repository;

import java.util.List;

import com.football.rankings.exception.HadoopConnectivityException;
import com.football.rankings.model.MatchesModel;
import com.football.rankings.model.RankingModel;


public interface RankingRepository {
	
	 List<RankingModel> getRankingList() throws HadoopConnectivityException;

	List<MatchesModel> getMatchesFromHadoop() throws HadoopConnectivityException;
}