package com.football.rankings.repository;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.football.rankings.annotation.MappingField;
import com.football.rankings.model.MatchesModel;
import com.football.rankings.util.Constants;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.api.java.JavaRDD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.football.rankings.exception.HadoopConnectivityException;
import com.football.rankings.model.RankingModel;

@Slf4j
@Component
public class RankingRepositoryImpl  implements RankingRepository{


	@Autowired
	BaseRepositoryImpl baseRepository;

	@Override
	@Cacheable(value = "ranks",key = "#root.methodName")
	public List<RankingModel> getRankingList() throws HadoopConnectivityException {

		String response = baseRepository.getSourceDataFromHadoop(Constants.globalRankingPath);
		String rankList[] = response.split("\n");
		List<RankingModel> rankingModelList = Arrays.asList(rankList).stream().skip(1).map(mapperFunction).collect(Collectors.toList());
		return rankingModelList;
	}

	@Override
	public List<MatchesModel> getMatchesFromHadoop() throws HadoopConnectivityException {
		try {
			JavaRDD<String> javaRDD = baseRepository.getSourceDataUsingSpark(Constants.globalMatchesPath);
			String header = javaRDD.first();
			String[] headers = header.split(",");
			javaRDD =javaRDD.filter(row -> !row.equalsIgnoreCase(header));

			long startTime = System.currentTimeMillis();
			JavaRDD<MatchesModel> matchesModelJavaRDD = javaRDD.map(line ->{
				String parts[] = line.split(",");
				MatchesModel matchesModel = new MatchesModel();
				for (int i = 0; i < parts.length; i++) {
					Field[] fields = matchesModel.getClass().getDeclaredFields();
					Field f = null;
					for(int j = 0 ; j < fields.length; j++){
						if(fields[j].isAnnotationPresent(MappingField.class)){
							String annotatedType = fields[j].getAnnotation(MappingField.class).value();
							if(annotatedType.equalsIgnoreCase(headers[i]))
							{
								f = fields[j];
							}
						}
						else if(fields[j].getName().equalsIgnoreCase(headers[i]))
						{
							f = fields[j];
						}
					}

					Class t =f.getType();
					f.setAccessible(true);
					if(t.getTypeName().equalsIgnoreCase("int")){
						f.set(matchesModel, Integer.parseInt(parts[i]));
					}
					else if(t.getTypeName().equalsIgnoreCase("double")){
						if(!parts[i].isEmpty())
							f.set(matchesModel, Double.parseDouble(parts[i]));

					}
					else
						f.set(matchesModel, parts[i]);

				}
				return matchesModel;
			});
			long endTime = System.currentTimeMillis();
			log.info("for loop time taken :::: ::::: ::::: :: " +String.valueOf(endTime- startTime));
			log.info(String.valueOf(matchesModelJavaRDD.count()));
			return matchesModelJavaRDD.collect();
		}
		catch (Exception e){
			log.error(e.getMessage());
		}
		return null;
	}


	private MatchesModel setMatchesModel(MatchesModel matchesModel, String[] parts,String[] headers) throws IllegalAccessException {
		for (int i = 0; i < parts.length; i++) {
			Field[] fields = matchesModel.getClass().getDeclaredFields();
			Field f = null;
			for(int j = 0 ; j < fields.length; j++){
				if(fields[j].isAnnotationPresent(MappingField.class)){
					String annotatedType = fields[j].getAnnotation(MappingField.class).value();
					if(annotatedType.equalsIgnoreCase(headers[i]))
					{
						f = fields[j];
					}
				}
				else if(fields[j].getName().equalsIgnoreCase(headers[i]))
				{
					f = fields[j];
				}
			}

			Class t =f.getType();
			f.setAccessible(true);
			if(t.getTypeName().equalsIgnoreCase("int")){
				f.set(matchesModel, Integer.parseInt(parts[i]));
			}
			else if(t.getTypeName().equalsIgnoreCase("double")){
				f.set(matchesModel, Double.parseDouble(parts[i]));

			}
			else
				f.set(matchesModel, parts[i]);

		}
		return matchesModel;
	}
	static Function<String,RankingModel> mapperFunction = (input) -> {
		RankingModel rankingModel = new RankingModel();
		String p[] = input.split(",");
		if(isNullOrEmpty(p[0]))
			rankingModel.setRank(Integer.valueOf(p[0]));
		if(isNullOrEmpty(p[1])) 
			rankingModel.setPrevRank(Integer.valueOf(p[1]));
		if(isNullOrEmpty(p[2]))
			rankingModel.setName(p[2]);
		if(isNullOrEmpty(p[3]))
			rankingModel.setLeague(p[3]);
		if(isNullOrEmpty(p[4]))
			rankingModel.setOff(Double.valueOf(p[4]));
		if(isNullOrEmpty(p[5]))
			rankingModel.setDef(Double.valueOf(p[5]));
		if(isNullOrEmpty(p[6]))
			rankingModel.setSpi(Double.valueOf(p[6]));
		return rankingModel;
	};

	static private boolean isNullOrEmpty(Object c) {
		if(c != null && (!(c instanceof String) ? true : !c.equals(""))) {
			return true;
		}
		return false;

	}


}
