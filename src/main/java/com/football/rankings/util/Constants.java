package com.football.rankings.util;

public class Constants {

    public static final String globalRankingPath = "spi_global_rankings.csv";

    public static final String globalMatchesPath = "spi_matches.csv";
}
