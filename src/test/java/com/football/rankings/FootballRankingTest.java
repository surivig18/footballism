package com.football.rankings;

import static org.testng.Assert.fail;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.football.rankings.exception.HadoopConnectivityException;
import com.football.rankings.model.RankingModel;
import com.football.rankings.repository.BaseRepositoryImpl;
import com.football.rankings.repository.RankingRepository;

@SpringBootTest(classes = FootBallRankingApplication.class)
public class FootballRankingTest extends AbstractTestNGSpringContextTests {

 @Autowired
 BaseRepositoryImpl baseRepositoryImpl;
 
 @Autowired
 RankingRepository rankingRepository;
 
  @Test
  public void testHadoopConnection() {
	  try {
		baseRepositoryImpl.getSourceDataFromHadoop("");
		fail();
	} catch (HadoopConnectivityException e) {
		
		Assert.assertEquals(e.getMessage(), e.getMessage());
	}
	  
  }
  @Test
  public void testNoDataFromSource() throws HadoopConnectivityException {
	  
	  try {
	  List<RankingModel> rankingList = rankingRepository.getRankingList();
	  Assert.assertTrue(rankingList == null || rankingList.size() > 0 );

	  }
	  catch(HadoopConnectivityException e) {
		  
	  }
  }
}
