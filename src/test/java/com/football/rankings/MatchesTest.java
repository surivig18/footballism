package com.football.rankings;

import com.football.rankings.config.HdfsConfig;
import com.football.rankings.elasticsearch.query.TeamsQuerySearch;
import com.football.rankings.model.MatchesModel;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.IntegerType;
import org.apache.spark.sql.types.StructType;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import scala.collection.Seq;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@Slf4j
@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class MatchesTest {

    @InjectMocks
    private TeamsQuerySearch teamsQuerySearch;

    private Dataset<Row> dataSet;
    @Before
    public void init()
    {

       try {

           System.setProperty("HADOOP_HOME", "/Users/sindusuria/dev/hadoop-3.2.1/bin");
           SparkSession spark = SparkSession.builder()
                   .appName("SparkStructuredStreamingDemo")
                   .master("local[2]")
                   .config("spark.driver.port" , 9091 ).
                   config("spark.ui.port",4041)
                   .getOrCreate();

           List<Row> rowList = new ArrayList<>();

           dataSet = spark.createDataFrame(
                   Arrays.asList(RowFactory.
                           create(Timestamp.valueOf("2017-09-13 00:00:00"), 1818, "Liverpool", "Sevilla FC", 1 , 0, "BPL",
                                   0.002,0.001,0.91,0.92,0.023,0.3,0.4,0.1,0.2,0.1,0.23,0.1,0.02,0.02,0.02)),
                   new StructType().
                           add("date", DataTypes.TimestampType).
                           add("leagueId", DataTypes.IntegerType).
                           add("team1", DataTypes.StringType).
                           add("team2", DataTypes.StringType).
                           add("score1",DataTypes.IntegerType).
                           add("score2",DataTypes.IntegerType).
                           add("league",DataTypes.StringType).
                           add("spi1",DataTypes.DoubleType).
                           add("spi2",DataTypes.DoubleType).
                           add("prob1",DataTypes.DoubleType).
                           add("prob2",DataTypes.DoubleType).
                           add("probtie",DataTypes.DoubleType).
                           add("projscore1",DataTypes.DoubleType).
                           add("projscore2",DataTypes.DoubleType).
                           add("importance1",DataTypes.DoubleType).
                           add("importance2",DataTypes.DoubleType).
                           add("xg1",DataTypes.DoubleType).
                           add("xg2",DataTypes.DoubleType).
                           add("nsxg1",DataTypes.DoubleType).
                           add("nsxg2",DataTypes.DoubleType).
                           add("adjscore1",DataTypes.DoubleType).
                           add("adjscore2",DataTypes.DoubleType)
                           );
       }
       catch(Exception e)
       {
           log.error(e.getMessage());
       }
    }

    @Test
    public void testElasticQueryForTeams()
    {
        List<MatchesModel> matchesModelList = teamsQuerySearch.transformDataSet(dataSet);
        log.info("testResult :: " + matchesModelList.size());
        long size = 0L;
        long listSize = matchesModelList.size();
        assertThat("transformation unit tests",
               listSize,
                greaterThan(size));
    }


}
